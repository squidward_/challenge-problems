# Input numbers
first_input = input().split()
num_skiers = int(first_input[0])
travel_time = int(first_input[1])
num_gondolas = int(first_input[2])

# Holds skier start times
skier_starts = []

for i in range(num_skiers):
    skier_starts.append(int(input()))

# Holds information about each gondolas next available time
gondolas = [0]*num_gondolas

# Holds the total wait time
total_wait = 0

skier_starts.sort()

print(skier_starts)

for skier in skier_starts:
    gondolas.sort()
    print(gondolas)

    # Gets the next available gondola
    gondola = gondolas[0]

    # There is wait time
    if gondola > skier:
        total_wait += gondola - skier

    gondolas[0] = skier + (travel_time*2)

print(total_wait)
