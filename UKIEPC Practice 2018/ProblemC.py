BeanTypes = int(input())

tmp = input().split(" ")
tmp = [int(a) for a in tmp]

NeededBeans = {}

for i in range(BeanTypes):
    if tmp[i] != 0:
        NeededBeans[i+1] = tmp[i]
        
NumFarms = int(input())

Farms = {}

for i in range(NumFarms):
    CrFarm = input().split(" ")
    
    del CrFarm[0]
    
    Farms[i+1] = [int(a) for a in CrFarm]
    
delQueue = []

for FarmID in Farms:
    rel = False
    for b in Farms[FarmID]:
        if b in NeededBeans.keys():
            rel = True
            break
    if rel == False:
        delQueue.append(FarmID)
        
for ID in delQueue:
    del Farms[ID]
    
KeyList = list(Farms.keys())[:]

KeyList.sort(key = lambda arr:len(Farms[arr]))

Cows = 0
for key in NeededBeans.keys():
    Cows+=NeededBeans[key]

for key in KeyList:
    if len(Farms[key]) != 1:
        break
    
    Cows-=NeededBeans[Farms[key][0]]
    
    
print(KeyList)