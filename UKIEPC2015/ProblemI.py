import math
import datetime

input_words = input().lower().split()
start = datetime.datetime.now()
hard_letters = "bcdgknpt"
hard_ends = "aou"
hard_endings = ["ah", "oh", "uh"]
hard_list = {}

meep = ""


for i in range(len(input_words)):
    word = input_words[i]
    # Calculate nearest hard letter
    dist = -1
    start_letter = ""
    for letter in hard_letters:
        if dist == -1 or math.fabs(ord(letter)-ord(word[0])) < dist:
            dist = math.fabs(ord(letter)-ord(word[0]))
            start_letter = letter

    newword = (i == 0 and start_letter.upper() or start_letter) + word[1:]

    cs = newword.split("-")
    balls = ""
    for j in range(len(cs)):
        cylable = cs[j]
        newcylable = cylable
        if j > 0:
            for k in range(len(cylable)):
                letter = cylable[k].lower()
                if letter in hard_letters:
                    newcylable = newcylable[:k]+start_letter+newcylable[k+1:]

        cylable = newcylable

        print("TEST "+cylable[-1])
        if j == len(cs)-1:
            if cylable[-1].lower() in hard_letters:
                print("HHHHHHHHHHHHHHHHHHHHHH")
                # Calculate nearest hard ending
                dist = -1
                ending = ""
                for letter in hard_ends:
                    if dist == -1 or math.fabs(ord(letter)-ord(cylable[-1].lower())) < dist:
                        dist = math.fabs(ord(letter)-ord(cylable[-1].lower()))
                        ending = letter
                ind = hard_ends.find(ending)
                cylable = cylable + hard_endings[ind]
            else:
                print("FAIL")

        print(cylable)
        balls += cylable

    print("# "+balls)
    meep += balls + " "


    #print(word[0], start_letter)

print(meep)
print("Took {}s".format(datetime.datetime.now()-start))
