###############################################

#Objects:

'''
Here all of the objects that we'll be using will be defined
and will be used throughout the program
'''

class Vertex():
    def __init__(self,x: int, y: int, Type: str, ID: int, exit=False):
        self.x = x
        self.y = y
        self.Type = Type
        self.ID = ID
        self.Edge = []
        self.exit = exit
        
    def getX(self):
        return self.x
    
    def getY(self):
        return self.y
    
    def getType(self):
        return self.Type
    
    def getID(self):
        return self.ID
    
    def appendEdge(self, edge):
        self.Edge.append(edge)
        
    def getEdges(self):
        return self.Edge
    
    def isExit(self):
        return self.exit
    
class Edge():
    def __init__(self,parent: Vertex, tgtVertex: Vertex, Weight: int):
        self.tgtVertex = tgtVertex
        self.Weight = Weight
        self.Parent = parent
        
    def getTgtVertex(self):
        return self.tgtVertex
    
    def getParent(self):
        return self.Parent
    
    def getWeight(self):
        return self.Weight

class PathPoint():
    def __init__(self, CurrentVertex: Vertex, PriorVertex: Vertex, edge: Edge):
        self.curVert = CurrentVertex
        self.priVert = PriorVertex
        self.edge = edge
        
    def getLength(self, Graph: dict, StartVertex: Vertex):
        length = 0
        
        lVert = self.priVert
        
        end = False
        while(end==False):
            length+=self.edge.getWeight()
        
        
        

##############################################

# Returns a vertex if its X and Y coordinate match that given to the function
def getVertexByCoord(x:int, y:int):
    for v in Vertexes:
        if (v.getX() == x and v.getY() == y):
            return v
           
    return None

def pathLength(Path: PathPoint, StartVertex: Vertex):
    print("of")

''' 
First step is to take all of the required inputs from the user in order to
appropriately build the node graph required.
'''

r,c=input().split(' ')
r=int(r)
c=int(c)

TextMap = [None]*r

for n in range(r):
    TextMap[n] = input()
    
CarY,CarX = input().split(' ')
CarY = int(CarY)-1
CarX = int(CarX)-1

Vertexes = []

# Populate vertex table
for i in range(r):
    for n in range(c):
        v = None
        if(TextMap[i][n] == '#'):
            continue
        elif(TextMap[i][n] == 'c'):
            v = Vertex(n, i, "car", len(Vertexes))
        elif(TextMap[i][n] == 'D'):
            # Check if this door is an exit (on the edge of the map)
            isExit = ( (i==0) or (i==r-1) or (n==0) or (n==c-1) )
            v = Vertex(n, i, "door", len(Vertexes), isExit)
        else:
            print("Something did done not good")
            continue
        
        Vertexes.append(v)

# Populate edges table of each vertex
for i in range(r):
    for n in range(c):
        if(TextMap[i][n] == "#"):
            continue
        v = getVertexByCoord(n,i)
        # Check Up
        if (i>0):
            if (TextMap[i-1][n] != "#"):
                w = 1
                if(TextMap[i-1][n] == 'D'):
                    w=0
                else:
                    w=1
                print(str(w) + " ("+str(i)+","+str(n)+") Up Check")
                e = Edge(v,getVertexByCoord(n,i-1), w)
                v.appendEdge(e)
        # Check Right
        if (n<c-1):
            if (TextMap[i][n+1] != "#"):
                w=1
                if(TextMap[i][n+1]=='D'):
                    w=0
                else:
                    w=1
                print(str(w) + " ("+str(i)+","+str(n)+") Right Check")
                e = Edge(v,getVertexByCoord(n+1,i), w)
                v.appendEdge(e)
        # Check Down
        if (i<r-1):
            if (TextMap[i+1][n] != "#"):
                w = 1
                if(TextMap[i+1][n] == 'D'):
                    w=0
                else:
                    w=1
                e = Edge(v,getVertexByCoord(n,i+1), w)
                print(str(w) + " ("+str(i)+","+str(n)+") Down Check")
                v.appendEdge(e)
        # Check Left
        if (n>0):
            if (TextMap[i][n-1] != "#"):
                w = 1
                if(TextMap[i][n-1] == 'D'):
                    w=0
                else:
                    w=1
                print(str(w) + " ("+str(i)+","+str(n)+") Left Check")
                e = Edge(v,getVertexByCoord(n-1,i), w)
                v.appendEdge(e)
                
# Do digestives algorithm
exitCount = 0
def digestives( curVert, curLen = 0, curRoute = [], indent="" ):
    global exitCount
    curRoute.append( curVert )
    # Store the currently shorest valid route
    curValidRoute = []
    curValidLength = -1
    # Loop through each connection
    for connection in curVert.getEdges():
        conn_vert = connection.getTgtVertex()
        # If the vertex has been visited, ignore it
        if conn_vert in curRoute:
            continue
        # If the connection leads to a door, end
        elif conn_vert.isExit():
            testLen = curLen + connection.getWeight()
            testRoute = curRoute.copy()
            testRoute.append( conn_vert )
            exitCount += 1
            if (curValidLength == -1) or (testLen < curValidLength):
                curValidLength = testLen
                curValidRoute = testRoute
        else:
            testLen, testRoute = digestives( conn_vert, curLen + connection.getWeight(), curRoute.copy(), indent+" " )
            # If the testlen has actually found an exit
            if testLen != -1:
                if (curValidLength == -1) or (testLen < curValidLength):
                    curValidLength = testLen
                    curValidRoute = testRoute
    return curValidLength, curValidRoute

def Djikstra(StartVertex: Vertex, AllVertexes: list):
    
    cEdge = StartVertex.getEdges()
    cEdge.sort(key=lambda i: i.getWeight)
    Solved = False
    
    dict = {}
    
    for Vert in AllVertexes:
        dict[Vert] = {0}
        
    while(Solved==False):
        dict[cEdge[0].getTgtVertex()] = cEdge.getParent()
        cEdge = cEdge + cEdge.getparent().getEdges()
        del cEdge[0]
        
    
    
        
length, route = digestives( getVertexByCoord(CarX, CarY) )
length+=1 # Account for starting node
print("{} potential routes were found.".format(exitCount))
print("The shortest route found is as follows:")
for i in route:
    print( "[{},{}] Type {}".format(i.x, i.y, i.Type) )
print("This route requires moving {} cars.".format(length))
        

from graphics import *

def toScr( p:int ):
    return 10+p*40

def main():
    win = GraphWin("Thing", 500, 500)
    #c = Circle(Point(50,50), 10)
    #c.draw(win)
    
    # Draw the points and connections
    for v in Vertexes:
        c = Circle(Point(toScr(v.x), toScr(v.y)), 10)
        c.setFill((v.Type == "car" and color_rgb(255,0,0) or (v.isExit() and color_rgb(0,255,0) or color_rgb(0,0,255) )))
        c.draw(win)
        
        for e in v.getEdges():
            v2 = e.getTgtVertex()
        
            l = Line( Point(toScr(v.x), toScr(v.y)), Point(toScr(v2.x), toScr(v2.y)) )
            l.draw(win)
            
    # Draw the route
    for i in range(len(route)-1):
        v1 = route[i]
        v2 = route[i+1]
        l = Line( Point(toScr(v1.x), toScr(v1.y)), Point(toScr(v2.x), toScr(v2.y)) )
        l.setFill(color_rgb(255, 255, 0))
        l.setWidth(4)
        l.draw(win)
    
    win.getMouse() # pause for click in window
    win.close()

main()
