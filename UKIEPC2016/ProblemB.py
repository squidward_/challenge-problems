import graphics as g

BulkMin = int(input("Minimum Bulkhead Area: "))

n = int(input())

class Point():
    def __init__(self,x, y):
        self.x = int(x)
        self.y = int(y)
        
    def __str__(self):
        return "("+str(self.x)+","+str(self.y)+")"
        
def getLength(Point1:Point, Point2:Point):
    return ((Point1.x-Point2.x)**2 + (Point1.y-Point2.y)**2)**.5

def getArea(Point1:Point,Point2:Point,Point3:Point):
    l1 = getLength(Point1, Point2)
    l2 = getLength(Point2, Point3)
    l3 = getLength(Point3, Point1)
    
    p = l1+l2+l3
    p/=2
    
    return (p*(p-l1)*(p-l2)*(p-l3))**.5

Vertices = [Point]*n
win = g.GraphWin("BO", 1000,1000)
for i in range(n):
    x,y = input().split(" ")
    Vertices[i] = Point(x,y)
    if i!=0:
        gP1 = g.Point(Vertices[i].x, Vertices[i].y)
        gP2 = g.Point(Vertices[i-1].x, Vertices[i-1].y)
        l = g.Line(gP1, gP2)
        l.draw(win)
    
TotArea = 0
for e in range(2,n):
    TotArea+=getArea(Vertices[e], Vertices[e-1], Vertices[0])

TotArea = TotArea.__round__(6)

M = int(TotArea // BulkMin)
print(M)

win.getMouse()


    


