
'''
This problem felt very ambiguous when reading it, it "All outputs must be accurate to
an absolute or relative acccuracy of at most 10e-6" whilst all of the sample inputs/outputs 
contained 7 decimal places. Whilst approaching this problem we decided to follow the given
outputs and work it out to 10e-7
'''

SeedPrice = float(input("Cost of Seed: ")) # Take the price of the seed

n = int(input("Number of lawns: ")) # Take the quantity of lawns

TotArea = 0.0

for i in range(n):
    w,l=input("Dimensions: ").split(" ") # For each lawn have the width and length inputted 
    w=float(w)
    l=float(l)
    TotArea+=w*l # Increase the area for each lawn

TotCost = float(TotArea*SeedPrice)
TotCost.__round__(7) # Calculate the cost and round it to 7 digits

print(str.format('{0:.7f}',TotCost)) # Format float to ensure 7 digits are displayed