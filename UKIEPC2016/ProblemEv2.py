############################################################
# First take the inputs from the user

# Line 1 is the rows and collumns

r,c = input().split(" ") # r and c are seperated by a space in the input string
r = int(r) # Cast both to integer types
c = int(c)

# The next input is r lines of the text map

TextMap = []

for n in range(r):
    TextMap.append(input(""))
# The final input to the program is the row and collumn of the desired car to move

r_car, c_car = input().split(" ")
r_car = int(r_car)-1# Shift by -1 to new relative coords interpretted in map
c_car = int(c_car)-1

############################################################
# Next we are going to define some objects for our graph representation

# Vertex will represent each of the nodes on the graph
class Vertex():
    def __init__(self, x:int, y:int, exit:bool=False):
        self.x = x # x and y will be used to reference
        self.y = y
        self.exit = exit

    def getX(self):
        return self.x

    def getY(self):
        return self.y

    def isExit(self):
        return self.exit

class Edge():
    def __init__(self, Child:Vertex, Parent:Vertex, Weight:int=0):
        self.Child = Child
        self.Parent = Parent
        self.Weight = Weight

    def getChild(self):
        return self.Child

    def getParent(self):
        return self.Parent

    def getWeight(self):
        return self.Weight

############################################################
# Helper functions

def isExit(x:int,y:int,r:int,c:int):
    return (x==0 or y==0 or x==(c-1) or y==(r-1))

############################################################
# Now we're going to iterate through the textmap inputted and build the graph

Graph = {}
StartVertex = None

for y in range(r): # x and y are inverted to standard cartesian modelling in problem
    for x in range(c):
        cur = TextMap[y][x]

        if(cur == '#'):
            continue #We don't consider wall objects

        v = Vertex(x, y, isExit(x, y, r, c)) # Declare current vertex

        if(x==c_car and y==r_car):
            StartVertex = v

        print("Vertex defined at ("+str(x)+","+str(y)+") exit="+str(v.isExit()))
        Edges = []

        #Connect upper node
        print("Checking UP : ("+str(x)+","+str(y)+")")
        if(y != 0 and TextMap[y-1][x] != '#'):
            UpperEdge = Edge(Vertex(x, y-1, isExit(x, y-1, r, c)), v,int(TextMap[y-1][x] != "D"))
            Edges.append(UpperEdge)
            print("Edge defined")

        #Connect right node
        print("Cheking RIGHT: ("+str(x+1)+","+str(y)+")")
        if(x != c-1 and TextMap[y][x+1] != '#'):
            RightEdge = Edge(Vertex(x+1, y, isExit(x+1, y, r, c)), v,int(TextMap[y][x+1] != "D"))
            Edges.append(RightEdge)
            print("Edge defined")

        #Connect Lower Node
        print("Checking DOWN: ("+str(x)+","+str(y+1)+")")
        if(y != c-1 and TextMap[y+1][x] != '#'):
            DownEdge = Edge(Vertex(x, y+1, isExit(x, y+1, r, c)), v,int(TextMap[y+1][x] != "D"))
            Edges.append(DownEdge)
            print("Edge defined")

        #Connect left node
        print("Checking LEFT: ("+str(x-1)+","+str(y)+")")
        if( x != 0 and TextMap[y][x-1] != "#"):
            LeftEdge = Edge(Vertex(x-1, y, isExit(x-1, y, r, c)), v,int(TextMap[y][x-1] != "D"))
            Edges.append(LeftEdge)
            print("Edge defined")

        # Connecting nodes have been defined, now add it to the graph

        Graph[v] = Edges


######################################################
# Now we start looking for the shortest route

Edges = Graph[StartVertex][:] # Shallow Copy edges from starting vertex

cVert = StartVertex # CurrentVertex (init start)
xEdges = [] # Explored Edges

Priors = {} # Node paths (Back track at end)
print("Starting loop")
solved = False
while(not solved):

    Edges.sort(key=lambda i: i.getWeight()) # Sort edges on weighting
    print("Edges sorted")
    nVert = Edges[0].getChild() # Next vertex (next closest edge)

    print("nVert = ", nVert)

    if(cVert in Priors.keys()):
        if(nVert in Priors.keys()):
            if((Priors[cVert][1]+Edges[0].getWeight()) < Priors[nVert][1]):
                Priors[nVert] = [cVert, Priors[cVert][1]+Edges[0].getWeight()]
                print("Path was shorter")
        else:
            Priors[nVert] = [cVert, Priors[cVert][1]+Edges[0].getWeight()]
            print("New path")
    else:
        Priors[nVert] = [cVert, Edges[0].getWeight()]
        print("First Node")

    if(nVert.isExit()):
        print("Exit found")
        break

    xEdges.append(Edges[0]) # Mark edge as explored

    del Edges[0] # Remove edge for consideration(s)

    print("Looping recursive")
    print(nVert.getX(), nVert.getY())
    fixedVert = None
    for v in Graph:
        if v.x == nVert.x and v.y == nVert.y:
            fixedVert = v
    for Edge in Graph[fixedVert]: # Extend edges that are under consideration to cVert's children

        print("In loop")

        if(Edge.getChild() != cVert and not xEdges.__contains__(Edge)): # Should not lead backwards and should not be explored

            Edges.append(Edge) # APPEND THAT BOI YA YEET

for Vert in Priors.keys():
    if(Vert.isExit()): #boobs
        print(Priors[Vert][1]+1)
