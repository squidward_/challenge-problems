# Set up dictionary of numbers
numbers = {}
for i in range(1, 10, 1):
    numbers[str(i)] = 0
    
numbers["0"] = 0

# Get the input
user_input = input(">>")

# Loop through every number
for number in user_input:
    try:
        numbers[number] += 1
    except:
        print("Entered an Invalid Number: {}".format(number))
    
for number in numbers:
    print("[{}] = {}".format(number, numbers[number]))
    
smallest_amt = -1
smallest_num = -1
for number in numbers:
    amount = numbers[number]
    if amount < smallest_amt or smallest_amt == -1:
        smallest_amt = amount
        smallest_num = number
        
print("The definate possible range is: 0-{}".format("9"*smallest_amt))

print("The smallest number will end in {}".format(smallest_num))

## IGNORE THIS
def canMakeIt(input_num):
    nums = {}
    for i in range(1, 10, 1):
        nums[str(i)] = 0
    nums["0"] = 0
        
    input_num = str(input_num)
    for num in input_num:
        nums[num] += 1
        
    for num in nums:
        if ( nums[num] > numbers[num] ):
            print("why")
            return False
            
    return True

if smallest_amt == 0 and smallest_num != "0":
    print("Smallest1 >> {}".format(smallest_num))
elif smallest_amt > 2:
    if smallest_num != "0":
        print("Smallest3 >> {}".format(smallest_num*(smallest_amt+1)))
    else:
        start = 10
        for number in numbers:
            if int(number) < start and number != "0":
                start = int(number)
        print("Smallest4 >> {}".format(str(start)+smallest_num*(smallest_amt+1)))
else:
    found = False
    i = int(str(1)+"0"*(smallest_amt-1)+str(smallest_num))
    while (not found):
        print("Testing {}".format(i))
        test = canMakeIt(i)
        if test == False:
            print("Smallest2 >> {}".format(i))
            found = True
        i += 10