
'''
This is ProblemH from the UKIEPC 

The idea of this problem is there is a number of inputs regarding the
rhyming of numerous words. 

Our solution for this was to build a list of potential suffixes that mean they do rhyme
and a second list of the words to be checked.

We'll iterate through each item in the first list, for each iteration we'll check to see 
if the given word for the given iteration ends with any of the suffixes provided with in
the second list. If it matches then YES will be outputted, if not NO will be outputted 
(Given there is no match in the list)

Input 1: 
The given word that wants to be checked for rhymes

Input 2:
Some integer value, n in the code, that represents the quantity of lists of rhyming endings

Input 3:
n lists that are the lists of rhyming endings.

Input 4:
Another integer value, n is reused, that represents the quantity of potential words to be checked

'''

RhymingWord = input("Rhyming Word: ") # Take the initial rhyme word

n = int(input("Number of lists of potential endings: ")) # Take the value n for amount of lists

PosEndings = [] # Possible endings 

for i in range(n): # Request user to enter n lines of possible endings
    PosEndings+=input("Endings: ").split(" ")

TestPhrases = []

n = int(input("Number of phrases to check: ")) # Ask for the amount of test phrases

for i in range(n): # Cycle through to get each test phrase
    TestPhrases.append(input("Phrase: "))
    
for Phrase in TestPhrases: # Iterate through the given test phrases
    Rhyme = False
    for Sfx in PosEndings: # Iterate through suffixes
        if Phrase.endswith(Sfx):
            Rhyme = True
            break 
    
    print(Rhyme and "YES" or "NO") #Output YES if TRUE, no if else