# Used to print timings
import datetime

# Get input width and height of graph
height, width = input(">> ").split(" ")
height = int(height)
width = int(width)

# Get the user's textmap input
textmap = []
for i in range(height):
    textmap.append(input("> "))

# Get the user's x and y of the car
car_y, car_x = input(">> ").split(" ")
car_y = int(car_y)-1
car_x = int(car_x)-1

# Records start time
start_time = datetime.datetime.now()

# Vertex class
class Vertex():
    def __init__(self, x:int, y:int, exit:bool=False):
        self.x = x
        self.y = y
        self.is_exit = exit
        self.edges = []
    def addEdge(self, edge):
        self.edges.append(edge)

# Edge class
class Edge():
    def __init__(self, start:Vertex, target:Vertex, Weight:int=0):
        self.start = start
        self.target = target
        self.weight = weight

# Finds if the node at XY is an exit
def isExit(x:int, y:int):
    return (x==0 or y==0 or x==(width-1) or y==(height-1))

# Returns the vertex at XY
def getVertex(x:int, y:int):
    for v in vertexes:
        if v.x == x and v.y == y:
            return v
    Exception("Couldn't find {}, {}".format(x, y))

# Add every vertex to the vertex list
vertexes = []
for x in range(width):
    for y in range(height):
        textmap_char = textmap[y][x]

        # Ignore it if it's a wall
        if (textmap_char == "#"):
            continue

        vertex = Vertex(x, y, isExit(x, y))

        vertexes.append( vertex )

# Loop through every vertex and build edges
for vertex in vertexes:
    x = vertex.x
    y = vertex.y

    print("Building Edges for ({}, {})".format(x, y))

    # Check up for connection
    if (y>0):
        if (textmap[y-1][x] != "#"):
            weight = (textmap[y-1][x] == "c" and 1 or 0)
            target = getVertex(x, y-1)
            edge = Edge(vertex, target, weight)
            vertex.addEdge(edge)
            print("Found Up: ({}, {})".format(x,y-1))
    # Check right for connection
    if (x<width-1):
        if (textmap[y][x+1] != "#"):
            weight = (textmap[y][x+1] == "c" and 1 or 0)
            target = getVertex(x+1, y)
            edge = Edge(vertex, target, weight)
            vertex.addEdge(edge)
            print("Found Right: ({}, {})".format(x+1,y))
    # Check down for connection
    if (y<height-1):
        if (textmap[y+1][x] != "#"):
            weight = (textmap[y+1][x] == "c" and 1 or 0)
            target = getVertex(x, y+1)
            edge = Edge(vertex, target, weight)
            vertex.addEdge(edge)
            print("Found Down: ({}, {})".format(x,y+1))
    # Check left for connection
    if (x>0):
        if (textmap[y][x-1] != "#"):
            weight = (textmap[y][x-1] == "c" and 1 or 0)
            target = getVertex(x-1, y)
            edge = Edge(vertex, target, weight)
            vertex.addEdge(edge)
            print("Found Left: ({}, {})".format(x-1,y))

# Now all the edges are built, do digestives
start_vertex = getVertex(car_x, car_y)
current_vertex = start_vertex
possible_edges = start_vertex.edges.copy()
visited_edges = []
visited_nodes = {}

solved = False
exit = None
while (not solved):
    # Sort the edges list by weighting
    possible_edges.sort(key=lambda e: e.weight)

    ## Print out edge list
    text = ""
    for edge in possible_edges:
        text += "({}, {})[{}] ".format(edge.target.x, edge.target.y, edge.weight)
    print(text)

    # Next vertex is at top of list
    next_edge = possible_edges[0]
    current_vertex = next_edge.start
    next_vertex = next_edge.target
    print("Targeting ({}, {}) from ({}, {})".format(next_vertex.x, next_vertex.y, current_vertex.x, current_vertex.y))

    # Check if this path has been explored before
    if (current_vertex in visited_nodes.keys()):
        if (next_vertex in visited_nodes.keys()):
            if (visited_nodes[current_vertex]["length"]+next_edge.weight < visited_nodes[next_vertex]["length"]):
                visited_nodes[next_vertex] = {
                    "length": visited_nodes[current_vertex]["length"]+next_edge.weight,
                    "previous": current_vertex
                }
                print("    Path to ({}, {}) found shorter".format(next_vertex.x, next_vertex.y))
            else:
                print("    Path to ({}, {}) is not shorter".format(next_vertex.x, next_vertex.y))
        else:
            visited_nodes[next_vertex] = {
                "length": visited_nodes[current_vertex]["length"]+next_edge.weight,
                "previous": current_vertex
            }
            print("    New path to ({}, {}) found".format(next_vertex.x, next_vertex.y))
    else:
        visited_nodes[next_vertex] = {
            "length": next_edge.weight,
            "previous": current_vertex
        }
        print("    ({}, {}) connects to start.".format(next_vertex.x, next_vertex.y))

    if (next_vertex.is_exit):
        print("    Found Exit at ({}, {})".format(next_vertex.x, next_vertex.y))
        solved = True
        exit = next_vertex
        break

    # Remove edge from consideration
    visited_edges.append(next_edge)
    del possible_edges[0]

    # Add current vertex edges to consiferation
    for edge in next_vertex.edges:
        # Make sure it's not already under consideration
        if edge in possible_edges:
            continue
        try:
            print("Appending edge {} {}".format(edge.target.x, edge.target.y))
        except:
            print("Error... Cur: {}, {}".format())
        possible_edges.append(edge)

print("\nEXIT ROUTE FOUND:")
cur_node = exit
while (cur_node != start_vertex):
    prev_node = visited_nodes[cur_node]["previous"]
    print("[{}] ({},{}) to ({},{})".format(visited_nodes[cur_node]["length"], cur_node.x, cur_node.y, prev_node.x, prev_node.y))
    cur_node = prev_node

print("Total Moved Cars: {}".format(visited_nodes[exit]["length"]+1))
print("Took {}s".format(datetime.datetime.now()-start_time))

# Graphics stuff
from graphics import *

# Translates local vector coords into screen coords
def c(z:int):
    r = 20
    return r + (r/2) + (r*2*z)

# Make a new window
window = GraphWin("Car Showroom", 500, 500)

# Draw every vertex
for vertex in vertexes:
    circle = Circle(Point(c(vertex.x), c(vertex.y)), 20)
    circle.setFill(isExit(vertex.x, vertex.y) and color_rgb(0, 255, 0) or color_rgb(0, 0, 255))
    circle.draw(window)
    for edge in vertex.edges:
        target = edge.target
        line = Line(Point(c(vertex.x), c(vertex.y)), Point(c(target.x), c(target.y)))
        line.draw(window)


# Draw the path
cur_node = exit
while (cur_node != start_vertex):
    prev_node = visited_nodes[cur_node]["previous"]
    p1 = Point(c(prev_node.x), c(prev_node.y))
    p2 = Point(c(cur_node.x), c(cur_node.y))
    line = Line(p1, p2)
    line.setArrow("last")
    line.setFill("yellow")
    line.draw(window)
    cur_node = prev_node

window.getMouse()
