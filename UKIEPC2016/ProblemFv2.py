'''
Problem: 	Given a set of numbers, return the smallest possible
			number that CAN'T be made using those numbers.
Example:	Input: 	0123456789
			Output: 11
Breakdown:	The smallest number that can't be made will always end
			in whatever number you have the least of.
			For example, if you only have three 4s but six of every
			other number, the smallest impossible number must end in
			all three 4s (444) plus one more (4444)
'''

import datetime

# Sets up a dictionary of numbers, 0-9
# 0 is added after as it will be considered the biggest number
input_numbers = {}
for i in range(1, 10, 1):
	input_numbers[i] = 0
input_numbers[0] = 0

# Gets the user's input
user_input = input(">> ")

start = datetime.datetime.now();

# Counts up all of the input numbers
# Ignores non-numbers to help with testing
for char in user_input:
	try:
		input_numbers[int(char)] += 1
	except:
		print("Invalid character entered: {}".format(char))

# Prints out the count of each number for debugging
for i in input_numbers:
	print("[{}] = {}".format(i, input_numbers[i]))

# Finds which of the numbers has the least entries
smallest_num = None
for i in input_numbers:
	amount = input_numbers[i]
	if (smallest_num == None):
		smallest_num = (i, amount)
	elif (smallest_num[1] > amount ):
		smallest_num = (i, amount)

# Prints out the smallest number
print("{} has the least amount of entries with {}.".format(smallest_num[0], smallest_num[1]))

# If the amount is 0, that number can't be made
# Also, make sure it's not 0 as that's not considered
if (smallest_num[1] == 0 and smallest_num[0] != 0):
	print(">> {}".format(smallest_num[0]))
# If the smallest number is 0, we need to precede it with a number
elif (smallest_num[0] == 0):
	# Find the smallest number to put before the 0s
	preceder = 0
	for i in input_numbers:
		if input_numbers[i] != 0:
			preceder = i
			break
	# If there are no other numbers, then 1 must be the smallest
	if preceder == 0:
		print(">> 1 cannot be made.")
	# Smallest number is the preceder followed by the 0s
	else:
		print(">> {}{} cannot be made.".format(preceder, str(smallest_num[0])*(smallest_num[1]+1)))
# Smallest number is n+1 of the least number
else:
	print(">> {} cannot be made.".format(str(smallest_num[0])*(smallest_num[1]+1)))

print("Took {}s".format(datetime.datetime.now()-start))
