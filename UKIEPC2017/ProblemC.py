n = int(input("")) # n represents the number of balls on the table

Points = {"black":7, "pink": 6, "blue":5, "brown":4, "green":3, "yellow":2, "red":1}
Balls = {b:0 for b in Points.keys()}

for i in range(n): # Collect the balls from the table
    ball = input()
    Balls[ball]+=1
    
MaxBall = None
for key in Balls.keys(): # Find highest scoring ball on the table
    if Balls[key] != 0:
        MaxBall = key
        break
    
FinalScore = 0

FinalScore+=(Points[MaxBall]*Balls["red"]) # Work out total points
if(not MaxBall == "red"):
    FinalScore+=Balls["red"]

Balls["red"]=0
for key in Points.keys():
    FinalScore+=Balls[key]*Points[key]

print(FinalScore)