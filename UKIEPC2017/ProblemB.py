import ConvexHull as cv 
'''
This problem is essentially asking us to find the shortest possible shadow of a 2d shape in any rotation

The approach for this problem is to first find the convex hull of all of the given vertices in order to 
account for any truely horrific shapes that could be inputted into the system.

Then iterate through each of the edges and find the length of the normal to the line that passes through the furthest point.

We then take the shortest route found for each edge 
'''

n = int(input("Vertice Count: "))

vList = []

for i in range(n):
    i,j = input().split(" ")
    i=int(i)
    j=int(j)
    v = cv.Vector(i, j)
    vList.append(v)
    
vList = cv.findConvexHull(vList)





