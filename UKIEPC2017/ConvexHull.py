'''
@author Pseudo // Mitchell Wright
@email mitchell.wright877@gmail.com
@github https://github.com/Pseudooo
'''

'''

Now in order to appropriately approach this problem I decided to instead of envisioning the coordinate pairs
as two numbers but to instead look at them as vectors. This makes no real difference but helps me to work better
when working with vectors between points.

'''

class Vector():
    def __init__(self, i:float,j:float):
        self.i = i
        self.j = j
    
    def getMag(self):
        return (self.i**2+self.j**2)**.5
    
    def __str__(self):
        return "("+str(self.i)+"i + "+str(self.j)+"j)"
    
    def __add__(self, other):
        return Vector(self.i+other.i, self.j+other.j)
    
    def __sub__(self, other):
        return Vector(self.i-other.i, self.j-other.j) 
    
    def __mul__(self, other):
        return (self.i*other.i)+(self.j*other.j)
    
def CrossProduct(Vector1: Vector, Vector2: Vector): # Return the cross product of two vectors
    return (Vector1.i*Vector2.j) - (Vector1.j*Vector2.i)

def sep(Vector1: Vector, Vector2: Vector, Points): # Return a list of points that are outside the line
    
    '''
    Usually the cross product operator is only defined in R^3 but for this case we can consider the vectors
    3-dimensional but when looking at two dimensional vectors, if their cross product (Assuming k component = 0)
    is positive it means that the item is within the given bound otherwise it is outside. And reversing the input
    vectors will inverse the result.
    '''
    
    return [p for p in Points if CrossProduct(p-Vector1, Vector2 - Vector1) < 0] 

def FindFurther(Vector1: Vector, Vector2: Vector, Points):
    if len(Points) == 0:
        return []
    
    '''
    This function finds the convexes recursively by calling apon itself, within itself.
    It simply locates the displacement vector that is furthest from the line and then 
    calls itself to find the two hulls within them
    '''
    
    MajorVec = min(Points, key=lambda p: CrossProduct(p - Vector1, Vector2 - Vector1))
    
    LeftSet = sep(MajorVec, Vector2, Points)
    RightSet = sep(Vector1, MajorVec, Points)
    
    return FindFurther(MajorVec, Vector2, LeftSet) + [MajorVec] + FindFurther(Vector1, MajorVec, RightSet)

def findConvexHull(PointList):
    
    '''
    This starts the process of finding the convex hull by finding the two extrema points
    (Highest and lowest points) and simply calling on the FindFurther function
    to find all of the intermediary convexes
    '''
    
    PointList.sort(key=lambda vec:vec.j)
    MinorVec = PointList[0]
    MajorVec = PointList[-1]
    
    LeftSet = sep(MinorVec, MajorVec, PointList)
    RightSet = sep(MajorVec, MinorVec, PointList)
    
    return [MajorVec] + FindFurther(MinorVec, MajorVec, LeftSet) + [MinorVec] + FindFurther(MajorVec, MinorVec, RightSet)


